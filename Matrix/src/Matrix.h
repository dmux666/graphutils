/*
 * Matrix.h
 *
 *  Created on: Nov 17, 2016
 *      Author: dmux
 */

#ifndef MATRIX_H_
#define MATRIX_H_
#include <iostream>

using namespace std;

class Matrix {
public:
    Matrix();
    Matrix(int x, int y);
    virtual ~Matrix();
    void print();
    int getValue(int x, int y);
    void setValue(int x, int y, int input);
    int getWidth() { return width; }
    int getHeight() { return height; }
    void zeroMatrix();
    void fillMatrix();

private:
    int** graph;
    int width;
    int height;
    const int DEFAULT_ARRAY_SIZE = 5;
    void initializeMatrix();
    void checkBounds(int x, int y);
};

#endif /* MATRIX_H_ */
