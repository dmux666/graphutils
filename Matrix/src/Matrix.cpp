/*
 * Matrix.cpp
 *
 *  Created on: Nov 17, 2016
 *      Author: dmux
 */

#include "Matrix.h"

Matrix::Matrix() {
    width = DEFAULT_ARRAY_SIZE;
    height = DEFAULT_ARRAY_SIZE;
    initializeMatrix();
}

Matrix::Matrix(int x, int y) {
    width = x;
    height = y;
    initializeMatrix();
    zeroMatrix();
}

Matrix::~Matrix() {
    for(int i = 0; i < width; i++) {
        delete graph[i];
    }
    delete graph;
}

void Matrix::print() {
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < height; j++) {
            cout << graph[i][j] << " ";
        }
        cout << endl;
    }
}

int Matrix::getValue(int x, int y) {
    checkBounds(x, y);
    return graph[x][y];
}

void Matrix::setValue(int x, int y, int input) {
    checkBounds(x, y);
    graph[x][y] = input;
}

void Matrix::initializeMatrix() {
    graph = new int* [width];
    for(int i = 0; i < width; i++) {
        graph[i] = new int[height];
    }
}

void Matrix::zeroMatrix() {
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < height; j++) {
            graph[i][j] = 0;
        }
    }
}

void Matrix::fillMatrix() {
    for(int i = 0; i < width; i++) {
        for(int j = 0; j < height; j++) {
            graph[i][j] = 1;
        }
    }
}

void Matrix::checkBounds(int x, int y) {
    if(x < 0 || x >= width
            || y < 0 || y >= height) {
        throw std::invalid_argument("requested location exceeds Matrix dimensions");
    }
}
