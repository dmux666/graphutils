/*
 * Graph.cpp
 *
 *  Created on: Nov 17, 2016
 *      Author: dmux
 */

#include "Graph.h"

Graph::Graph() {
    matrix = new Matrix();
    numberOfVertices = matrix->getWidth();
}

Graph::Graph(int numberVertices) {
    numberOfVertices = numberVertices;
    matrix = new Matrix(numberVertices, numberVertices);
}

Graph::~Graph() {
    delete matrix;
}

void Graph::setEdge(int a, int b) {
    matrix->setValue(a, b, 1);
}

void Graph::deleteEdge(int a, int b) {
    matrix->setValue(a, b, 0);
}

void Graph::fillAllEdges() {
    matrix->fillMatrix();
}

void Graph::deleteAllEdges() {
    matrix->zeroMatrix();
}

int Graph::getDegree(int a) {
    int degree = 0;
    for(int i = 0; i < numberOfVertices; i++) {
        degree = degree + matrix->getValue(a, i);
    }
    return degree;
}

bool Graph::isComplete() {
    bool result = true;
    for(int i = 0; i < matrix->getWidth(); i++) {
        for(int j = 0; j <matrix->getHeight(); j++) {
            if(matrix->getValue(i, j) == 0) {
                result = false;
            }
        }
    }
    return result;
}

bool Graph::isAdjacent(int a, int b) {
    return !(matrix->getValue(a, b) == 0);
}
