/*
 * Graph.h
 *
 *  Created on: Nov 17, 2016
 *      Author: dmux
 */

#ifndef GRAPH_H_
#define GRAPH_H_
#include "Matrix.h";

class Graph {
public:
    Graph();
    Graph(int numberVertices);
    virtual ~Graph();
    void setEdge(int a, int b);
    int getDegree(int a);
    void deleteEdge(int a, int b);

    //TODO implementthese
    int* getAdjacentVertices(int a);
    //this is an int array of the columns that have a 1
    bool isAdjacent(int a, int b);

    void fillAllEdges();
    void deleteAllEdges();

private:
    int numberOfVertices;
    Matrix* matrix;
    bool isComplete();
};

#endif /* GRAPH_H_ */
